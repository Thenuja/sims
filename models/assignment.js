const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const assignmentSchema = new Schema({
    courseId:{
        type:String,
        required:true
    },
    name:{
        type:String,
        required:true
    },
    dueDate:{
        type:Date,
        required:true
    },
    assignments:{
        type:Array
        
    }
});

module.exports = mongoose.model('assignment',assignmentSchema);