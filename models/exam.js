const mongoose= require('mongoose');
const Schema = mongoose.Schema;
const question = require('./question');

const examSchema = new Schema({
    courseid:{
        type:String,
        required:true
    },
    name:{
        type:String,
        required:true
    },
    dueDate:{
        type:Date,
        required:true
    },
    questions:{
        type:Array,
        ref:question
    }
});

module.exports=mongoose.model('exam',examSchema);
