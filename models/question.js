const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const questionSchema = new Schema({
    courseId:{
        type:String,
        required:true
    },
    ques:{
        type:String,
        required:true
    },
    answers:{
        type:Array,
        required:true
    },
    correctAnswer:{
        type:String,
        required:true
    }
});

module.exports = mongoose.model('question',questionSchema);