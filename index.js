const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const examRoute = require('./routes/exam.server.routes');
const questionRoute =  require('./routes/question.server.routes');
const assignmentRoute =  require('./routes/assignment.server');

const app = express();

const configure = require('./config/configure');

mongoose.connect(configure.DB,{useNewUrlParser:true}).then(
    ()=>{console.log("successfully connected to the database")},
    err=>{console.log("Error connecting to the database"+err)})

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());
app.use(cors());

app.use('/exam',examRoute);
app.use('/question',questionRoute);
app.use('/assignment',assignmentRoute);

const PORT = process.env.PORT || 8000
app.listen(PORT,()=>{
    console.log(`Server has started on port:${PORT}`);
});