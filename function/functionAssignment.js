const assignment = require('../models/assignment');

module.exports.getAssignment = function(courseId,callback){
    assignment.find({courseId}).select('assignments').exec().then(function(data){
        callback(null,data);
    }).catch(function(err){
        callback(err);
    });
}

// module.exports.markAssignment = function(courseId,grade,studentNo,assignments){
//     return new Promise(function(resolve,reject){
//         assignment.find({courseId:courseId}).select('assignments').exec().then(function(data){
//             if(data.length!=0){
//                 let status;
//                 for(let i=0 ; i<data[0].length; i++){
//                     studentNo
//                         assignment.update({studentNo:studentNo},{"$push":{"grade":grade}}).exec().then(function(){
//                             resolve({message:"updated marks"});
//                         });
                    
//                 }
//             }
//         });
//     });
// }

module.exports.markAssignment=function(courseId,studentNo,grade){
    assignment.find({courseId:courseId}).select('assignments').exec().then(function(data){
        if(data.length!=0){
            assignment.update({studentNo:studentNo},{"$set":{"assignments.$.grade":grade}})
        }
    });
}

// module.exports.markAssignment = function(courseId,grade,studentNo){
//    assignment.update({courseId:courseId, assignments:{studentNo:studentNo}},{$set:{grade:grade}},(err,doc)=>{
//        if(err){
//            console.log(err);
//            return;
//        }
//        if(doc){
//            console.log(doc);
//            return;
//        }
//    });
// }