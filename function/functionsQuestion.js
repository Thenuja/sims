const express = require('express');
const question = require('../models/question');

module.exports.createQuestion = function(QuestionInfo, callback){
    let createQues = new question({
        courseId:QuestionInfo.courseId,
        ques:QuestionInfo.ques,
        answers:QuestionInfo.answers,
        correctAnswer:QuestionInfo.correctAnswer

    });

    createQues.save(function(err,data){
        if(err){
            callback(err);
            return;
        }
        callback(null,"successfully added");
    });
}

module.exports.getQuestion = function(courseId,callback){
    question.find({courseId:courseId}).exec().then(function(data){
        // if(data.length==0){
        //     callback("Question are not ready for this course");
        // }else{
            callback(null,data);
        // }
    }).catch(function(err){
        callback(err);
    })
}