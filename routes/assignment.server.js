const express = require('express');
const router = express.Router();
const assignment = require('../models/assignment');
const functions = require('../function/functionAssignment');

router.post('/create',function(req,res){
    const assignmentObj = new assignment(req.body);
    assignmentObj.save().then(data=>{
        console.log("successfully create assignment "+req.body.name);
        res.status(200).json(data);
    }).catch(err=>{
        res.status(400).json(err);
    });
});

router.get('/',function(req,res){
    assignment.find().then(data=>{
        return res.status(200).json(data);
    }).catch(err=>{
        return res.status(500).json(err);
    });
});

router.get('/find/:id', function(req,res){
    assignment.findById(req.params.id).then(data=>{
        return res.status(200).json(data);
    }).catch(err=>{
        return res.status(500).json(err);
    });
});

router.put('/update/:id',function(req,res){
    assignment.updateOne({_id:req.params.id},req.body).then(doc=>{
        return res.status(200).json(doc);
    }).catch(err=>{
        return res.status(500).json(err);
    })
});


//view students aassignment
router.get('/find/:courseId/assignments', function(req,res){
    functions.getAssignment(req.params.courseId, function(err,data){
        if(!err){
            res.status(200).json(data);
        }else{
            res.status(400).json(err);
        }
    }); 
});

router.put('/find/:courseId/assignments',function(req,res){
    functions.markAssignment(req.params.courseId, function(err,data){
        if(!err){
            res.status(200).json(data);
        }else{
            res.status(400).json(err);
        }
    });
})

// router.put('/find/:courseId',function(req,res){
//     functions.markAssignment(req.params.courseId, function(err,data){})
// })

module.exports = router;