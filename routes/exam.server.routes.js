const express = require('express');
const router = express.Router();
const exam = require('../models/exam');

router.post('/create',function(req,res){
    const examObj = new exam(req.body);
    examObj.save().then(data=>{
        res.status(200).json(data);
    }).catch(err=>{
        res.status(400).json(err);
    });
});

router.get('/',function(req,res){
    exam.find().then(data=>{
        return res.status(200).json(data);
    }).catch(err=>{
        return res.status(500).json(err);
    });
});

router.get('/find/:id', function(req,res){
    exam.findById(req.params.id).then(data=>{
        return res.status(200).json(data);
    }).catch(err=>{
        return res.status(500).json(err);
    });
});

router.put('/update/:id',function(req,res){
    exam.updateOne({_id:req.params.id},req.body).then(doc=>{
        return res.status(200).json(doc);
    }).catch(err=>{
        return res.status(500).json(err);
    })
});

module.exports = router;