const express = require('express');
const router = express.Router();
const question = require('../models/question');
const functions = require('../function/functionsQuestion');

router.post('/create',function(req,res){
    functions.createQuestion(req.body, function(err,data){
        if(!err){
            res.status(200).send({"success ":data});
        }else{
            res.status(400).send({"error ":err});
        }
    })
});

router.get('/find/:courseId',function(req,res){
    functions.getQuestion(req.params.courseId, function(err,data){
        if(!err){
            res.status(200).send({"success":data});
        }else{
            res.status(400).send({"error ":err});
        }
    });
});

// router.post('/create',function(req,res){
//     const quesObj = new question(req.body);
//     quesObj.save().then(data=>{
//         res.status(200).json(data);
//     }).catch(err=>{
//         res.status(400).json(err);
//     });
// });

router.get('/',function(req,res){
    question.find().then(data=>{
        return res.status(200).json(data);
    }).catch(err=>{
        return res.status(500).json(err);
    });
});

// router.get('/find/:courseId/courseId', function(req,res){
//     question.findById(req.params.type).then(data=>{
//         return res.status(200).json(data);
//     }).catch(err=>{
//         return res.status(500).json(err);
//     });
// });

// router.get('/find/:courseId/courseId', function(req,res){
//     functions.getCourseId(req.params.courseId, function(err,data){
//         if(!err){
//             res.status(200).send({"sucess":data});
//         }else{
//             res.status(400).send({"error":err});
//         }
//     })
// })
module.exports = router;